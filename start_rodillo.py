''' tk_image_slideshow3.py
create a Tkinter image repeating slide show
tested with Python27/33  by  vegaseat  03dec2013
'''
from PIL import Image, ImageTk, ExifTags
import io
import sys
import requests
import time
import json
import os
import subprocess
import socket
from threading import Thread
from itertools import cycle
try:
    # Python2
    import Tkinter as tk
except ImportError:
    # Python3
    import tkinter as tk
class App(tk.Tk):
    '''Tk window/label adjusts to size of image'''
    def __init__(self, image_files, x, y, delay):
        
        # the root will be self
        tk.Tk.__init__(self)
        self.attributes("-fullscreen", True)
        self.configure(background='black')

        # set x, y position only
        #self.geometry('+{}+{}'.format(x, y))
        self.delay = delay
        # allows repeat cycling through the pictures
        # store as (img_object, img_name) tuple
        #self.pictures = cycle((self.photo_image(image), image)
                              #for image in image_files)
        self.image_files = image_files
        self.picture_display = tk.Label(self, borderwidth=0)
        subprocess.Popen(['/etc/rodillo/rodillo_menu'])
        self.picture_display.pack()

        self.bind('<Escape>', self.quit_prog)
        self.bind('<Button-1>', self.start_menu)

    def show_slides(self):
        '''cycle through the images and show them'''
        # next works with Python26 or higher
        #img_object, img_name = next(self.pictures)
        global counter
        if counter>=5:
            t = Thread(target=self.update_list, args=([counter]))
            t.start()
        else:
            counter=counter+1
        global current
        img_object = self.photo_image(self.image_files[current])

        self.picture_display.config(image=img_object)
        self.picture_display.image = img_object

        # shows the image filename, but could be expanded
        # to show an associated description of the image
        #self.title(img_name)
        self.after(self.delay, self.show_slides)
        print('current vor Erhoehung ' + str(current))
        current = current + 1
        if current >= len(self.image_files):
            current = 0
        print('current nach Erhoehung ' + str(current))
    def run(self):
        self.mainloop()

    def photo_image(self, jpg_filename):
        print(jpg_filename)
        with io.open(jpg_filename, 'rb') as ifh:
            pil_image = Image.open(ifh)
            for orientation in ExifTags.TAGS.keys() : 
                if ExifTags.TAGS[orientation]=='Orientation' : break 
            try :
                exif=dict(pil_image._getexif().items())
                if exif[orientation] == 3 : 
                    pil_image=pil_image.rotate(180, expand=True)
                elif exif[orientation] == 6 : 
                    pil_image=pil_image.rotate(270, expand=True)
                elif exif[orientation] == 8 : 
                    pil_image=pil_image.rotate(90, expand=True)
            except:
                pass
            


            pil_image.thumbnail((self.winfo_screenwidth(), self.winfo_screenheight()), Image.ANTIALIAS)
            return ImageTk.PhotoImage(pil_image)

    def quit_prog(self, event):
        sys.exit()

    def start_menu(self, event):
        UDP_IP = "127.0.0.1"
        UDP_PORT = 9234
        MESSAGE = "open"
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))

    def update_list(self,i):
        try:
            print("Update-Thread started...")
            rodillopath='/etc/rodillo/'
            resp = requests.get('https://rodillo.bytewish.de/list_pictures.php', cert=(rodillopath + 'client.pem', rodillopath + 'client.key'))
            new_imagelist = []
            #print(resp.status_code)
            if resp.status_code == 200:
                jlist = json.loads(resp.text)
                for knocke in jlist["images"]:
                    tstring = rodillopath + "cache/" + str(knocke) + ".jpg"
                    if not os.path.isfile(tstring):
                        print(tstring + " is not there. Downloading...")
                        dlurl = "https://rodillo.bytewish.de/get_picture.php?imageId=" + str(knocke)
                        print(dlurl)
                        global current
                        current = -1
                        print('current ' + str(current))
                        r = requests.get(dlurl, cert=(rodillopath + 'client.pem', rodillopath + 'client.key'), stream=True)
                        print(r.status_code)
                        if r.status_code == 200:
                            with open(tstring, 'wb') as f:
                                for chunk in r.iter_content(1024):
                                    f.write(chunk)
                    new_imagelist.append(tstring)
                self.image_files = new_imagelist
            global counter
            counter = 0
        except Exception:
            print("Unexpected error:", sys.exc_info()[0])
            pass

current = 0
counter = 0
# set milliseconds time between slides

delay = 3500
# get a series of gif images you have in the working folder
# or use full path, or set directory to where the images are
#image_files = [
#'/etc/rodillo/cache/dummy.jpg'
#]

image_files = []
cache_files = os.listdir('/etc/rodillo/cache')
for cache_file in cache_files:
        image_files.append('/etc/rodillo/cache/' + cache_file)

# upper left corner coordinates of app window
x = 100
y = 50
app = App(image_files, x, y, delay)

app.show_slides()
app.run()
