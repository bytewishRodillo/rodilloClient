#include "networkconnectiondialogip.h"
#include "ui_networkconnectiondialogip.h"
#include <QStyle>
#include <QDesktopWidget>
#include <QHostAddress>
#include <QPalette>
#include <QMessageBox>
#include <QProcess>

NetworkConnectionDialogIP::NetworkConnectionDialogIP(QWidget *parent, QString interface, QString ssid, QString password, QString encryption) :
    QDialog(parent),
    ui(new Ui::NetworkConnectionDialogIP)
{
    ui->setupUi(this);
    m_parentWindow = parent;
    m_interface = interface;
    m_ssid = ssid;
    m_pwd = password;
    m_encryption = encryption;

    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->setGeometry(
        QStyle::alignedRect(
            Qt::RightToLeft,
            Qt::AlignTop,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
    m_networkConfig = new BWNetworkConfiguration();
    connect(ui->btnBack, SIGNAL(clicked(bool)), this, SLOT(openParentWindow()));

    // Sprachen-Kram
    if(UI_LANGUAGE == "deu") {
        ui->groupBox->setTitle("IP-Einstellungen");
        ui->btnBack->setText("Zurück");
        ui->btnSave->setText("Speichern");
        ui->groupIP->setTitle("Statische IP");
        ui->optManual->setText("Feste IP-Adresse");
        ui->optAutomatic->setText("Automatische IP-Adresse");
        ui->txtIP->setPlaceholderText("IP-Adresse");
        ui->txtDNS1->setPlaceholderText("DNS-Server 1");
        ui->txtDNS2->setPlaceholderText("DNS-Server 2");
        ui->txtGateway->setPlaceholderText("Standardgateway");
        ui->txtNetmask->setPlaceholderText("Netzmaske");
    }
}

NetworkConnectionDialogIP::~NetworkConnectionDialogIP()
{
    delete ui;
}

void NetworkConnectionDialogIP::openParentWindow()
{
    m_parentWindow->show();
    this->close();
}


void NetworkConnectionDialogIP::on_optAutomatic_clicked()
{
    if(ui->optAutomatic->isChecked()) {
        ui->groupIP->setEnabled(false);
    } else {
        ui->groupIP->setEnabled(true);
    }
}

void NetworkConnectionDialogIP::on_optManual_clicked()
{
    if(ui->optAutomatic->isChecked()) {
        ui->groupIP->setEnabled(false);
    } else {
        ui->groupIP->setEnabled(true);
    }
}

void NetworkConnectionDialogIP::on_btnSave_clicked()
{
    if(QString::compare(m_interface, "eth0") == 0) m_networkConfig->disableWifi();
    if(QString::compare(m_interface, "wlan0") == 0) {
            QString *ssid = new QString(m_ssid);
            QString *pass = new QString(m_pwd);
            m_networkConfig->connectToWifi(ssid, pass, true);
    }
    m_networkConfig->removeStaticIps();
    bool allValid = true;

    if(ui->optManual->isChecked()) {
        // Manuelle IP-Konfiguration
        // Prüfen, ob Einträge valide sind
        QHostAddress address_ip(ui->txtIP->text());
        QHostAddress address_netmask(ui->txtNetmask->text());
        QHostAddress address_gateway(ui->txtGateway->text());
        QHostAddress address_dns1(ui->txtDNS1->text());
        QHostAddress address_dns2(ui->txtDNS2->text());

        QPalette palette;
        palette.setColor(QPalette::Base,Qt::red);

        if(!address_ip.protocol() == QAbstractSocket::IPv4Protocol) {
            ui->txtIP->setPalette(palette);
            allValid = false;
        }

        //if(!address_netmask.protocol() == QAbstractSocket::IPv4Protocol) {
        //    ui->txtNetmask->setPalette(palette);
         //   allValid = false;
        //}

        if(!address_gateway.protocol() == QAbstractSocket::IPv4Protocol) {
            ui->txtGateway->setPalette(palette);
            allValid = false;
        }

        if(!address_dns1.protocol() == QAbstractSocket::IPv4Protocol) {
            ui->txtDNS1->setPalette(palette);
            allValid = false;
        }

        if(!address_dns2.protocol() == QAbstractSocket::IPv4Protocol) {
            ui->txtDNS2->setPalette(palette);
            allValid = false;
        }

        if(allValid) {
            m_networkConfig->setStaticIp(m_interface, ui->txtIP->text(), ui->txtGateway->text(), ui->txtNetmask->text(), ui->txtDNS1->text(), ui->txtDNS2->text());
        }

    }

    if(allValid) {
        QMessageBox msgbox;
        if(UI_LANGUAGE == "deu") {
            msgbox.setText("Die Verbindungseinstellungen wurden gespeichert. Das Gerät startet jetzt neu.");
        } else {
            msgbox.setText("The network setup is complete. The device will reboot now.");
        }
        msgbox.exec();

        QProcess::startDetached("sudo shutdown -r now");
    }

}
