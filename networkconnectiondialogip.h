#ifndef NETWORKCONNECTIONDIALOGIP_H
#define NETWORKCONNECTIONDIALOGIP_H

#include <QDialog>
#include "bwnetworkconfiguration.h"
#include "bwglobals.h"

namespace Ui {
class NetworkConnectionDialogIP;
}

class NetworkConnectionDialogIP : public QDialog
{
    Q_OBJECT

public:
    explicit NetworkConnectionDialogIP(QWidget *parent = 0, QString interface = "wlan0", QString ssid = "", QString password = "", QString encryption = "");
    ~NetworkConnectionDialogIP();

public slots:
    void openParentWindow();

private slots:
    void on_optAutomatic_clicked();

    void on_optManual_clicked();

    void on_btnSave_clicked();

private:
    Ui::NetworkConnectionDialogIP *ui;
    QWidget *m_parentWindow;
    QString m_interface;
    BWNetworkConfiguration *m_networkConfig;

    QString m_ssid;
    QString m_pwd;
    QString m_encryption;
};

#endif // NETWORKCONNECTIONDIALOGIP_H
