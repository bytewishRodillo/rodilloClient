#ifndef NETWORKCONNECTIONDIALOGMAIN_H
#define NETWORKCONNECTIONDIALOGMAIN_H

#include <QDialog>
#include "bwglobals.h"

namespace Ui {
class NetworkConnectionDialogMain;
}

class NetworkConnectionDialogMain : public QDialog
{
    Q_OBJECT

public:
    explicit NetworkConnectionDialogMain(QWidget *parent = 0);
    ~NetworkConnectionDialogMain();

public slots:
    void openParentWindow();

private slots:
    void on_btnConnectWifi_clicked();

    void on_btnConnectLAN_clicked();

private:
    Ui::NetworkConnectionDialogMain *ui;
    QWidget *m_parentWindow;
};

#endif // NETWORKCONNECTIONDIALOGMAIN_H
