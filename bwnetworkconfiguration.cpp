#include "bwnetworkconfiguration.h"
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QDateTime>
#include <QDebug>
#include <QProcess>

BWNetworkConfiguration::BWNetworkConfiguration(QObject *parent) : QObject(parent)
{
#if defined(WIN32)
   m_wpa_file = "C:\\bytewish\\wpa_supplicant.conf";
   m_dhcpc_file = "C:\\bytewish\\dhcpcd.conf";
#else
   m_wpa_file = "/etc/wpa_supplicant/wpa_supplicant.conf";
   //m_wpa_file = "/data/rodillo/wpa_supplicant/wpa_supplicant.conf";
   //m_dhcpc_file = "/etc/dhcpcd.conf";
   m_dhcpc_file = "/data/rodillo/dhcpcd.conf";
   m_dhcpc_destfile = "/etc/dhcpcd.conf";
#endif
}

bool BWNetworkConfiguration::connectToWifi(QString *ssid, QString *password, bool overWriteAllExisting)
{
    QList<BWWirelessNetwork*> *templist = new QList<BWWirelessNetwork*>;
    QList<BWWirelessNetwork*> *newlist = new QList<BWWirelessNetwork*>;
    templist = getAllWirelessNetworks();

    BWWirelessNetwork *tempNetwork;
    if (templist->count() > 0 && overWriteAllExisting == false) {
        bool networkWasAdded = false;
        for(int i=0; i<templist->count(); i++) {
            tempNetwork = templist->at(i);
            if(QString::compare(tempNetwork->ssid(), ssid, Qt::CaseInsensitive) == 0) {
                // Network is present in configuration
                tempNetwork->setPriority(0);
                tempNetwork->setPassword(*password);
                networkWasAdded = true;
            } else {
                tempNetwork->setPriority(100);
            }
            newlist->append(tempNetwork);
        }
        if(!networkWasAdded) {
            tempNetwork = new BWWirelessNetwork(*ssid, *password);
            tempNetwork->setPriority(0);
            newlist->append(tempNetwork);
        }
    } else {
        tempNetwork = new BWWirelessNetwork(*ssid, *password);
        newlist->append(tempNetwork);
    }

    writeConfigToFile(newlist);
    return reconfigureWifi();
}

bool BWNetworkConfiguration::removeWifi(QString *ssid)
{
    QList<BWWirelessNetwork*> *templist = new QList<BWWirelessNetwork*>;
    QList<BWWirelessNetwork*> *newlist = new QList<BWWirelessNetwork*>;

    templist = getAllWirelessNetworks();
    bool networkWasRemoved = false;
    BWWirelessNetwork *tempNetwork;
    for(int i=0; i<templist->count(); i++) {
        tempNetwork = templist->at(i);
        qDebug() << tempNetwork->ssid() << "<->" << *ssid;
        if(QString::compare(tempNetwork->ssid(), *ssid, Qt::CaseInsensitive) == 0) {
            qDebug() << "REMOVED!";
            networkWasRemoved = true;
        } else {
            newlist->append(tempNetwork);
        }
    }

    writeConfigToFile(newlist);
    return networkWasRemoved;
}

bool BWNetworkConfiguration::disableWifi()
{
    // Backup copy
    //QString backupfile = m_wpa_file + QString::number(QDateTime::currentMSecsSinceEpoch()) + ".bak";
    //QFile::copy(m_wpa_file, backupfile);

    QString filename = m_wpa_file;
    QFile file(filename);
    if (file.open(QIODevice::WriteOnly|QIODevice::Truncate)) {
        QTextStream stream(&file);
        // Write header
        stream << "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev" << endl;
        stream << "update_config=1" << endl;
        stream << "country=DE" << endl << endl;
    }
    file.close();
    return true;
}

QList<BWWirelessNetwork*>* BWNetworkConfiguration::getAllWirelessNetworks()
{
    QList <BWWirelessNetwork*> *tempList = new QList <BWWirelessNetwork*>;
    QFile inputFile(m_wpa_file);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       bool isInNetwork = false;


       BWWirelessNetwork* tempNetwork;
       while (!in.atEnd())
       {
          QString line = in.readLine();
          line = line.trimmed();
          if(QString::compare(line, "network={", Qt::CaseInsensitive) == 0) {
              isInNetwork = true;
              tempNetwork = new BWWirelessNetwork();
          }

          if(QString::compare(line, "}", Qt::CaseInsensitive) == 0) {
              isInNetwork = false;
              tempList->append(tempNetwork);
          }

          if(isInNetwork) {
              if(line.indexOf("ssid=", 0, Qt::CaseInsensitive) >= 0) {
                  QString section = line.section("=", 1);
                  if(section.indexOf("\"", 0, Qt::CaseInsensitive) >= 0) {
                      section = section.section("\"", 1);
                      section = section.section("\"", 0, 0);
                  }
                  tempNetwork->setSsid(section);
              } else if(line.indexOf("psk=", 0, Qt::CaseInsensitive) >= 0) {
                  QString section = line.section("=", 1);
                  if(section.indexOf("\"", 0, Qt::CaseInsensitive) >= 0) {
                      section = section.section("\"", 1);
                      section = section.section("\"", 0, 0);
                  }
                  tempNetwork->setPassword(section);
              } else if(line.indexOf("key_mgmt=", 0, Qt::CaseInsensitive) >= 0) {
                  QString section = line.section("=", 1);
                  if(section.indexOf("\"", 0, Qt::CaseInsensitive) >= 0) {
                      section = section.section("\"", 1);
                      section = section.section("\"", 0, 0);
                  }
                  tempNetwork->setEncryption(section);
              } else if(line.indexOf("priority=", 0, Qt::CaseInsensitive) >= 0) {
                  QString section = line.section("=", 1);
                  if(section.indexOf("\"", 0, Qt::CaseInsensitive) >= 0) {
                      section = section.section("\"", 1);
                      section = section.section("\"", 0, 0);
                  }
                  tempNetwork->setPriority(section.toInt());
              }


          }
       }
       inputFile.close();
    }
    return tempList;
}

bool BWNetworkConfiguration::writeConfigToFile(QList<BWWirelessNetwork *> *listOfWirelessNetworks)
{
    // Backup copy
    //QString backupfile = m_wpa_file + QString::number(QDateTime::currentMSecsSinceEpoch()) + ".bak";
    //QFile::copy(m_wpa_file, backupfile);

    QString filename = m_wpa_file;
    QFile file(filename);
    if (file.open(QIODevice::WriteOnly|QIODevice::Truncate)) {
        QTextStream stream(&file);
        // Write header
        stream << "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev" << endl;
        stream << "update_config=1" << endl;
        stream << "country=DE" << endl << endl;

        for(int i=0; i<listOfWirelessNetworks->count(); i++) {
            qDebug() << "WRITE " << QString::number(i);
            BWWirelessNetwork *tempNetwork = listOfWirelessNetworks->at(i);
            stream << "network={" << endl;
            tempNetwork->setEncryption("WPA-PSK");
            stream << "\t" << "ssid=\"" << tempNetwork->ssid() << "\"" << endl;
            stream << "\t" << "psk=\"" << tempNetwork->password() << "\"" << endl;
            stream << "\t" << "key_mgmt=" << tempNetwork->encryption() << endl;
            if(tempNetwork->priority() >=0) stream << "\t" << "priority=" << tempNetwork->priority() << endl;

            stream << "}" << endl << endl;
        }
    }

    file.close();
    return true;
}

QStringList* BWNetworkConfiguration::scanWirelessNetworks()
{
    QProcess wifiProcess;
    QString exe = "sudo iwlist wlan0 scan";
    wifiProcess.start(exe);
    wifiProcess.waitForFinished();
    QString output(wifiProcess.readAllStandardOutput());
    QStringList *returnlist = new QStringList();
    if(output.indexOf("          Cell", 0, Qt::CaseInsensitive) != -1) {
        // Es wurde mindestens ein WLAN gefunden
        QStringList split1 = output.split("          Cell", QString::SkipEmptyParts);
        for(int i=0; i<split1.count(); i++) {
            QString tstring = split1.at(i);
            if(tstring.indexOf("                    ESSID", 0, Qt::CaseInsensitive) != -1) {
                // Es handelt sich um einen gültigen WLAN-Eintrag
                QStringList tlist = tstring.split("                    ESSID", QString::SkipEmptyParts);
                QString twlan = tlist.at(1);
                QStringList tlist2 = twlan.split(QRegExp("[\r\n]"), QString::SkipEmptyParts);
                QString returnwifi = tlist2.at(0);
                returnwifi = returnwifi.mid(2, returnwifi.count() - 3);

                // Dopplungen vermeiden
                if(!returnlist->contains(returnwifi, Qt::CaseInsensitive)) {
                    returnlist->append(returnwifi);
                }

            }
        }
    }

    return returnlist;
}

QString BWNetworkConfiguration::getConnectedWifi()
{
    QProcess wifiProcess;
    QString exe = "sudo iwconfig";
    wifiProcess.start(exe);
    wifiProcess.waitForFinished();
    QString output(wifiProcess.readAllStandardOutput());
    QString tempresult = "";
    if((output.indexOf("ESSID:", 0, Qt::CaseInsensitive) != -1) && (output.indexOf("unassociated", 0, Qt::CaseInsensitive) == -1)) {
        // Wir sind mit enem WLAN verbunden
        QStringList split1 = output.split("ESSID:\"", QString::SkipEmptyParts);
        tempresult = split1.at(1);
        QStringList tlist2 = tempresult.split(QRegExp("[\r\n]"), QString::SkipEmptyParts);
        tempresult = tlist2.at(0);
        tempresult = tempresult.replace("\"", "");
    } else {
        tempresult = "";
    }

    return tempresult;
}

bool BWNetworkConfiguration::reconfigureWifi()
{
    QString cmd = "sudo wpa_cli -i wlan0 reconfigure";
    QProcess::startDetached(cmd);
    return true;
}

bool BWNetworkConfiguration::restartNetworking()
{
    QString cmd = "sudo service networking restart";
    QProcess::startDetached(cmd);
    return true;
}

bool BWNetworkConfiguration::setStaticIp(QString p_interface, QString p_ip, QString p_gateway, QString p_subnetmask, QString p_dns1, QString p_dns2)
{
    // Backup copy
    //QString backupfile = m_dhcpc_file + QString::number(QDateTime::currentMSecsSinceEpoch()) + ".bak";
    //QFile::copy(m_dhcpc_file, backupfile);

    QString filename = m_dhcpc_file;
    QFile file(filename);
    if (file.open(QIODevice::WriteOnly|QIODevice::Truncate)) {
        QTextStream stream(&file);
        // Write header
        stream << "hostname" << endl;
        stream << "clientid" << endl;
        stream << "persistent" << endl;
        stream << "option rapid_commit" << endl;
        stream << "option domain_name_servers, domain_name, domain_search, host_name" << endl;
        stream << "option classless_static_routes" << endl;
        stream << "option ntp_servers" << endl;
        stream << "option interface_mtu" << endl;
        stream << "require dhcp_server_identifier" << endl;
        stream << "slaac private" << endl << endl;

        stream << "interface " << p_interface << endl;
        stream << "static ip_address=" << p_ip << "/" << toCidr(p_subnetmask.toLocal8Bit().data()) << endl;
        stream << "static routers=" << p_gateway << endl;
        stream << "static domain_name_servers=" << p_dns1 << " " << p_dns2 << endl;

        file.close();
    }

    // Kopiere Datei zum Ziel (andernfalls bewirkt sie nicht viel...)
    QString cmd = "sudo cp /data/rodillo/dhcpcd.conf /etc/dhcpcd.conf";
    QProcess::startDetached(cmd);
    return true;

}

bool BWNetworkConfiguration::removeStaticIps()
{
    // Backup copy
    //QString backupfile = m_dhcpc_file + QString::number(QDateTime::currentMSecsSinceEpoch()) + ".bak";
    //QFile::copy(m_dhcpc_file, backupfile);

    QString filename = m_dhcpc_file;
    QFile file(filename);
    if (file.open(QIODevice::WriteOnly|QIODevice::Truncate)) {
        QTextStream stream(&file);
        // Write header
        stream << "hostname" << endl;
        stream << "clientid" << endl;
        stream << "persistent" << endl;
        stream << "option rapid_commit" << endl;
        stream << "option domain_name_servers, domain_name, domain_search, host_name" << endl;
        stream << "option classless_static_routes" << endl;
        stream << "option ntp_servers" << endl;
        stream << "option interface_mtu" << endl;
        stream << "require dhcp_server_identifier" << endl;
        stream << "slaac private" << endl << endl;

        file.close();
    }

    QString cmd = "sudo cp /data/rodillo/dhcpcd.conf /etc/dhcpcd.conf";
    QProcess::startDetached(cmd);

    return true;
}

unsigned short BWNetworkConfiguration::toCidr(char* ipAddress)
  {
      unsigned short netmask_cidr;
      int ipbytes[4];

      netmask_cidr=0;
      sscanf(ipAddress, "%d.%d.%d.%d", &ipbytes[0], &ipbytes[1], &ipbytes[2], &ipbytes[3]);

      for (int i=0; i<4; i++)
      {
          switch(ipbytes[i])
          {
              case 0x80:
                  netmask_cidr+=1;
                  break;

              case 0xC0:
                  netmask_cidr+=2;
                  break;

              case 0xE0:
                  netmask_cidr+=3;
                  break;

              case 0xF0:
                  netmask_cidr+=4;
                  break;

              case 0xF8:
                  netmask_cidr+=5;
                  break;

              case 0xFC:
                  netmask_cidr+=6;
                  break;

              case 0xFE:
                  netmask_cidr+=7;
                  break;

              case 0xFF:
                  netmask_cidr+=8;
                  break;

              default:
                  return netmask_cidr;
                  break;
          }
      }

      return netmask_cidr;
  }
