#include <QStyle>
#include <QDesktopWidget>
#include <QDebug>
#include <QMessageBox>
#include <QProcess>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QRegularExpression>

#include "slideshowsettingsdialog.h"
#include "ui_slideshowsettingsdialog.h"

SlideshowSettingsDialog::SlideshowSettingsDialog(QWidget *parent, QWidget *p_welcomescreen) :
    QDialog(parent),
    ui(new Ui::SlideshowSettingsDialog)
{
    ui->setupUi(this);
    m_welcomescreen = p_welcomescreen;
    m_saveClicked = false;
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignTop,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );

    connect(ui->btnBack, SIGNAL(clicked(bool)), this, SLOT(openWelcomeWindow()));
    connect(ui->btnSave, SIGNAL(clicked(bool)), this, SLOT(saveSettings()));

    // Sprachen
    if(UI_LANGUAGE == "deu") {
        ui->btnBack->setText("Zurück");
        ui->btnSave->setText("Speichern");
        ui->lblRemoveAfter->setText("Bilder löschen nach");
        ui->lblRemoveAfter2->setText("Tagen (max 14)");
        ui->lblSlideshowName->setText("Diashow-Name");
        ui->lblSlideshowSpeed->setText("Geschwindigkeit");
        ui->lblSlideshowSpeed2->setText("Sekunden pro Bild");
        ui->lblSlideshowNameExample->setText("wird z.B. in Apps angezeigt");
    }

    m_networkconfig = new BWNetworkConfiguration();

    loadCertificates();

    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

    // Aktuelle Einstellungen lesen
    QNetworkRequest request;
    request.setSslConfiguration(m_sslConfiguration);

    request.setUrl(QUrl("https://rodillo.bytewish.de/list_pictures.php"));
    m_manager->get(request);

    QString cmd = "/usr/bin/xvkbd -geometry +1+265";
    QProcess::startDetached(cmd);

}

SlideshowSettingsDialog::~SlideshowSettingsDialog()
{
    delete ui;
}

void SlideshowSettingsDialog::openWelcomeWindow()
{
    QString cmd = "killall xvkbd";
    QProcess::startDetached(cmd);
    m_welcomescreen->show();
    this->close();
}

void SlideshowSettingsDialog::loadCertificates()
{
        QString certlocation = "";
        #if defined(WIN32)
           certlocation = "C:\\bytewish\\";
        #else
           certlocation = "/data/rodillo/";
        #endif

        // DEVICE CERT
        QFile certFile1(certlocation + "client.pem");
        certFile1.open(QFile::ReadOnly);
        m_deviceCertificate = new QSslCertificate(&certFile1);
        //qWarning() << m_deviceCertificate->isNull();
        certFile1.close();

        // DEVICE KEY
        QFile keyFile(certlocation + "client.key");
        keyFile.open(QFile::ReadOnly);
        m_deviceKey = new QSslKey(&keyFile, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
        //qWarning() << m_deviceKey->isNull();
        keyFile.close();

        // SIGNING CA CERT
        QFile certFile(certlocation + "sign_ca.pem");
        certFile.open(QFile::ReadOnly);
        m_signingCACertificate = new QSslCertificate(&certFile);
        //qWarning() << m_signingCACertificate->isNull();
        certFile.close();

        // REMOTE CA CERT
        QFile certFile2(certlocation + "remote_ca.pem");
        certFile2.open(QFile::ReadOnly);
        m_remoteCACertificate = new QSslCertificate(&certFile2);
        //qWarning() << m_remoteCACertificate->isNull();
        certFile2.close();

        // REMOTE INTERMEDIATE CA CERT
        QFile certFile3(certlocation + "remote_intermediate_ca.pem");
        certFile3.open(QFile::ReadOnly);
        m_remoteIntermediateCACertificate = new QSslCertificate(&certFile3);
        //qWarning() << m_remoteIntermediateCACertificate->isNull();
        certFile3.close();

        // Build SSL Configuration
        QSslConfiguration configuration;

        configuration.setLocalCertificate(*m_deviceCertificate);
        configuration.setPrivateKey(*m_deviceKey);

        QList<QSslCertificate> certs;
        certs.append(*m_signingCACertificate);
        certs.append(*m_remoteCACertificate);
        certs.append(*m_remoteIntermediateCACertificate);
        configuration.setCaCertificates(certs);

        m_sslConfiguration = configuration;
}

void SlideshowSettingsDialog::replyFinished(QNetworkReply *reply)
{
    if(reply->error()) {
        // Es ist ein Fehler aufgetreten
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
        QString messageHeader = "";
        QString messageText = "";
        QMessageBox msgbox;
        if(UI_LANGUAGE == "deu") {
            if(m_saveClicked) {
                m_saveClicked = false;
                ui->btnSave->setEnabled(true);
                messageText = "Die Einstellungen konnten nicht gespeichert werden: " + reply->errorString();
            } else {
                messageText = "Die Einstellungen konnten nicht gelesen werden: " + reply->errorString();
            }
            messageHeader = "Fehler bei der Verbindung";
        } else {
            if(m_saveClicked) {
                m_saveClicked = false;
                ui->btnSave->setEnabled(true);
                messageText = "Could not save settings: " + reply->errorString();
            } else {
                messageText = "Could not read current settings: " + reply->errorString();
            }
            messageHeader = "Connection error";
        }
        msgbox.setText(messageText);
        msgbox.setWindowTitle(messageHeader);
        msgbox.exec();
    } else {
        QString replysource = reply->readAll();
        qDebug() << replysource;
        QJsonDocument doc = QJsonDocument::fromJson(replysource.toUtf8());
        QJsonObject json = doc.object();
        if(!m_saveClicked) {
            // Antwort auf das Lesen der Einstellungen
            QJsonValue json_settings = json.value(QString("settings"));
            QJsonObject json_settings_o = json_settings.toObject();
            QJsonValue json_showname = json_settings_o.value(QString("name"));
            QJsonValue json_delay = json_settings_o.value(QString("delay"));
            QJsonValue json_retention = json_settings_o.value(QString("retention"));
            ui->txtSlideshowName->setText(json_showname.toString());
            int sl_speed = json_delay.toInt();
            sl_speed = sl_speed / 1000; // Umwandlung DB<->UI (ms<->s)
            ui->txtSlideshowSpeed->setText(QString::number(sl_speed));

            int sl_retention = json_retention.toInt();
            sl_retention = sl_retention / 24; // Umwandlung DB<->UI (Stunden<->Tage)
            ui->txtRemoveAfter->setText(QString::number(sl_retention));

            ui->btnSave->setEnabled(true);
        } else {
            m_saveClicked = false;
            // Antwort auf das Schreiben der Einstellungen
            QJsonValue json_root = json.value(QString("result"));
            QJsonObject json_root_o = json_root.toObject();
            QJsonValue json_result = json_root_o.value(QString("errorcode"));
            QJsonValue json_errorstring = json_root_o.value(QString("errorstring"));

            qDebug() << QString::number(json_result.toInt());
            qDebug() << json_errorstring.toString();

            if(json_result.toInt() == 0) {
                // Einstellungen wurden erfolgreich gespeichert, kein Fehlercode
                this->openWelcomeWindow();
            } else {
                // Einstellungen konnten aus irgend einem Grund nicht gespeichert werden
                ui->btnSave->setEnabled(true);
                QString messageHeader = "";
                QString messageText = "";
                QMessageBox msgbox;
                if(UI_LANGUAGE == "deu") {
                    messageHeader = "Fehler beim Speichern";
                    messageText = "Es ist ein Fehler (" + QString::number(json_result.toInt()) + ") beim Speichern aufgetreten: " + json_errorstring.toString();
                } else {
                    messageHeader = "Save error";
                    messageText = "Error (" + QString::number(json_result.toInt()) + ") while saving settings: " + json_errorstring.toString();
                }
                msgbox.setWindowTitle(messageHeader);
                msgbox.setText(messageText);
                msgbox.exec();
            }
        }
    }

        reply->deleteLater();
}

void SlideshowSettingsDialog::saveSettingsSubmit(QString showName, QString showDelay, QString showRetention)
{
    qDebug() << "Speichern...";
    m_saveClicked = true;
    // Einstellungen an Server übermitteln
    QNetworkRequest request;
    request.setSslConfiguration(m_sslConfiguration);
    QString baseUrl = "https://rodillo.bytewish.de/save_settings.php";
    QString urlParams = "?name=" + showName + "&delay=" + showDelay + "&retention=" + showRetention;
    QString completeUrl = baseUrl + urlParams;
    request.setUrl(QUrl(completeUrl));
    m_manager->get(request);
    ui->btnSave->setEnabled(false);
}

void SlideshowSettingsDialog::saveSettings()
{

    QString tShowName = ui->txtSlideshowName->text().replace("&", "").trimmed();
    QString tShowDelay = ui->txtSlideshowSpeed->text().trimmed();
    QString tShowRetention = ui->txtRemoveAfter->text().trimmed();

    QString messageHeader = "";
    QString messageText = "";
    QMessageBox msgbox;
    if(tShowName.isEmpty() || tShowDelay.isEmpty() || tShowRetention.isEmpty()) {
        // Mindestens eins der Textfelder ist leer

        if(UI_LANGUAGE == "deu") {
            messageHeader = "Fehler beim Speichern";
            messageText = "Mindestens eins der Textfelder ist leer! Bitte trage alle Daten ein.";
        } else {
            messageHeader = "Saving error";
            messageText = "At least one of the text inputs is empty. Please fill in all information.";
        }

        msgbox.setWindowTitle(messageHeader);
        msgbox.setText(messageText);
        msgbox.exec();
    } else {
        // In allen Textfeldern steht etwas. Sind die beiden letzten auch numerisch?
        bool isNumber_delay = false;
        bool isNumber_retention = false;
        int value = tShowDelay.toInt(&isNumber_delay);
        value = tShowRetention.toInt(&isNumber_retention);

        if(!(isNumber_delay & isNumber_retention)) {
            if(UI_LANGUAGE == "deu") {
                messageHeader = "Fehler beim Speichern";
                messageText = "Geschwindigkeit und Löschzeitraum müssen Nummern sein. Bitte korrigieren.";
            } else {
                messageHeader = "Saving error";
                messageText = "Speed and Retention have to be numeric values. Please check.";
            }
            msgbox.setWindowTitle(messageHeader);
            msgbox.setText(messageText);
            msgbox.exec();
        } else {
            // Alles OK, speichern
            this->saveSettingsSubmit(tShowName, tShowDelay, tShowRetention);
        }

    }
}
