#ifndef SLIDESHOWSETTINGSDIALOG_H
#define SLIDESHOWSETTINGSDIALOG_H

#include <QDialog>
#include "bwglobals.h"
#include <QtNetwork/QNetworkAccessManager>
#include <QSsl>
#include <QSslKey>
#include <QNetworkReply>
#include "bwnetworkconfiguration.h"

namespace Ui {
class SlideshowSettingsDialog;
}

class SlideshowSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SlideshowSettingsDialog(QWidget *parent = nullptr, QWidget *p_welcomescreen = nullptr);
    ~SlideshowSettingsDialog();

private:
    Ui::SlideshowSettingsDialog *ui;
    QWidget *m_welcomescreen;
    BWNetworkConfiguration *m_networkconfig;

    QNetworkAccessManager *m_manager;
    QSslConfiguration m_sslConfiguration;
    QSslCertificate *m_deviceCertificate;
    QSslKey *m_deviceKey;
    QSslCertificate *m_remoteCACertificate;             // CA-Certificate of the connected URL (the server certificate ca for https://services.bytewish.de)
    QSslCertificate *m_remoteIntermediateCACertificate; // Intermediate CA certificate (for example the startssl intermediate certificate)
    QSslCertificate *m_signingCACertificate;            // Public CA cert of the bytewish device ca
    bool m_saveClicked;                                   // Wir speichern hier, wenn gespeichert wurde, damit wir die Antwort entsprechend auswerten können

public slots:
    void openWelcomeWindow();

private slots:
    void loadCertificates();
    void replyFinished(QNetworkReply *reply);
    void saveSettingsSubmit(QString showName, QString showDelay, QString showRetention);
    void saveSettings();
};

#endif // SLIDESHOWSETTINGSDIALOG_H
