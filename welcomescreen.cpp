#include "welcomescreen.h"
#include "ui_welcomescreen.h"

#include <QStyle>
#include <QDesktopWidget>
#include <QDebug>
#include <QProcess>
#include <QTimer>
#include <QUrlQuery>
#include <QFileInfo>
#include <QFile>
#include <QDir>


WelcomeScreen::WelcomeScreen(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WelcomeScreen)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );


//    lastUrlWasFilelist = false;
    connect(ui->btnShutdown, SIGNAL(clicked(bool)), this, SLOT(openShutdownWindow()));
    connect(ui->btnSettings, SIGNAL(clicked(bool)), this, SLOT(openSettingsWindow()));
    connect(ui->btnShowQR, SIGNAL(clicked(bool)), this, SLOT(openQrCodeWindow()));
    connect(ui->btnSettingsSlideshow, SIGNAL(clicked(bool)), this, SLOT(openSlideshowSettingsWindow()));

    // Sprachenweiche
    QString sprache_deutsch = "/data/rodillo/lang_deu";
    if(QFileInfo::exists(sprache_deutsch) && QFileInfo(sprache_deutsch).isFile()) UI_LANGUAGE = "deu";

    // Sprachen
    if(UI_LANGUAGE == "deu") {
        ui->btnShowQR->setText("QR-Code anzeigen");
        ui->btnSettings->setText("Systeminstellungen");
        ui->btnSettingsSlideshow->setText("Diashow-Einstellungen");
        ui->btnShutdown->setText("Herunterfahren");
        ui->btnCloseMenu->setText("Menü schließen");
    }


    m_udpServer = new QUdpSocket(this);
    m_udpServer->bind(QHostAddress::LocalHost, 9234);
    connect(m_udpServer, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));


//    loadCertificates();
//    readVersion();



//    m_manager2 = new QNetworkAccessManager(this);
//    connect(m_manager2, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished2(QNetworkReply*)));

//    updateClient();

}


WelcomeScreen::~WelcomeScreen()
{
    delete ui;
}

//void WelcomeScreen::loadCertificates()
//{
//        QString certlocation = "";
//        #if defined(WIN32)
//           certlocation = "C:\\bytewish\\";
//        #else
//           certlocation = "/data/rodillo/";
//        #endif

//        // DEVICE CERT
//        QFile certFile1(certlocation + "client.pem");
//        certFile1.open(QFile::ReadOnly);
//        m_deviceCertificate = new QSslCertificate(&certFile1);
//        //qWarning() << m_deviceCertificate->isNull();
//        certFile1.close();

//        // DEVICE KEY
//        QFile keyFile(certlocation + "client.key");
//        keyFile.open(QFile::ReadOnly);
//        m_deviceKey = new QSslKey(&keyFile, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
//        //qWarning() << m_deviceKey->isNull();
//        keyFile.close();

//        // SIGNING CA CERT
//        QFile certFile(certlocation + "sign_ca.pem");
//        certFile.open(QFile::ReadOnly);
//        m_signingCACertificate = new QSslCertificate(&certFile);
//        //qWarning() << m_signingCACertificate->isNull();
//        certFile.close();

//        // REMOTE CA CERT
//        QFile certFile2(certlocation + "remote_ca.pem");
//        certFile2.open(QFile::ReadOnly);
//        m_remoteCACertificate = new QSslCertificate(&certFile2);
//        //qWarning() << m_remoteCACertificate->isNull();
//        certFile2.close();

//        // REMOTE INTERMEDIATE CA CERT
//        QFile certFile3(certlocation + "remote_intermediate_ca.pem");
//        certFile3.open(QFile::ReadOnly);
//        m_remoteIntermediateCACertificate = new QSslCertificate(&certFile3);
//        //qWarning() << m_remoteIntermediateCACertificate->isNull();
//        certFile3.close();

//        // Build SSL Configuration
//        QSslConfiguration configuration;

//        configuration.setLocalCertificate(*m_deviceCertificate);
//        configuration.setPrivateKey(*m_deviceKey);

//        QList<QSslCertificate> certs;
//        certs.append(*m_signingCACertificate);
//        certs.append(*m_remoteCACertificate);
//        certs.append(*m_remoteIntermediateCACertificate);
//        configuration.setCaCertificates(certs);

//        m_sslConfiguration = configuration;
//}



void WelcomeScreen::on_btnCloseMenu_clicked()
{
    this->hide();
}

void WelcomeScreen::openShutdownWindow()
{
    myShutdownDialog = new ShutdownDialog(0, this);
    myShutdownDialog->show();
    this->hide();

}

void WelcomeScreen::openSettingsWindow()
{
    mySettingsDialog = new SettingsDialog(0, this);
    mySettingsDialog->show();
    this->hide();
}

void WelcomeScreen::openSlideshowSettingsWindow()
{
    mySlideshowSettingsDialog = new SlideshowSettingsDialog(0, this);
    mySlideshowSettingsDialog->show();
    this->hide();
}

void WelcomeScreen::openQrCodeWindow()
{
    myQrCodeDialog = new QrCodeDialog(0, this);
    myQrCodeDialog->show();
    this->hide();
}


//void WelcomeScreen::readVersion()
//{
//    QString currentPath = QFileInfo(QCoreApplication::applicationFilePath()).absolutePath();
//    QString versionfilename = currentPath + "/version.txt";
//    QFile versionfile(versionfilename);

//    if(!versionfile.open(QIODevice::ReadOnly)) {
//        ui->lblVersion->setText("Version: 0.0.0");
//    } else {
//        QTextStream in(&versionfile);
//        QString line = in.readLine();
//        QStringList list1 = line.split(';');
//        QString versionbuild = list1[1];
//        VERSION_BUILD = versionbuild.toInt();

//        ui->lblVersion->setText("Version: " + list1[1]);
//    }
//}

//void WelcomeScreen::updateClient()
//{
//    // Ermitteln, ob Version aktuell ist
//    QNetworkRequest request;
//    request.setSslConfiguration(m_sslConfiguration);

//    request.setUrl(QUrl("https://rodillo.bytewish.de/updates/version.php"));
//    m_manager2->get(request);

//    // Weiter in replyFinished2
//}

//void WelcomeScreen::replyFinished2(QNetworkReply *reply)
//{
//    if(reply->error())
//        {
//            qDebug() << "ERROR!";
//            qDebug() << reply->errorString();

//            //QTimer::singleShot(m_loginTryInterval * 1000, this, SLOT(doLogin()));
//        }
//        else
//        {
//            //qDebug() << reply->header(QNetworkRequest::ContentTypeHeader).toString();
//            //qDebug() << reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();
//            //qDebug() << reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
//            //qDebug() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
//            //qDebug() << reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
//    //        qDebug() << "HAHN";
//    //        qDebug() << reply->readAll();
//    //        qDebug() << "HUHN";

//            // Command request was successful
//            // Check if there is anything to process in the reply
//            QString replysource = reply->readAll();
//            if(!lastUrlWasFilelist) {
//                int new_version = replysource.trimmed().toInt();
//                if(new_version > VERSION_BUILD) {
//                    // Es gibt eine neuere Version
//                    downloadAndProcessUpdate(new_version);
//                }
//            } else {
//                lastUrlWasFilelist = false;
//                QString currentPath = QFileInfo(QCoreApplication::applicationFilePath()).absolutePath();
//                QString filelistpath = currentPath + "/updates/files.txt";
//                QFile file(filelistpath);

//                file.open(QIODevice::WriteOnly);
//                file.write(replysource.toLocal8Bit());

//                file.close();

//                downloadUpdateFiles(filelistpath);
//            }


//        }

//        reply->deleteLater();
//}

//void WelcomeScreen::downloadAndProcessUpdate(int pVersion)
//{
//    qDebug() << "Downloading Version " << QString::number(pVersion);
//    QString currentPath = QFileInfo(QCoreApplication::applicationFilePath()).absolutePath();
//    QString updatepath = currentPath + "/updates/";

//    QDir dir(updatepath);
//    if (!dir.exists()) {
//        dir.mkpath(".");
//    }

//    dir.setNameFilters(QStringList() << "*.*");
//    dir.setFilter(QDir::Files);
//    foreach(QString dirFile, dir.entryList())
//    {
//        dir.remove(dirFile);
//    }

//    lastUrlWasFilelist = true;
//    QNetworkRequest request;
//    request.setSslConfiguration(m_sslConfiguration);

//    request.setUrl(QUrl("https://rodillo.bytewish.de/updates/" + QString::number(pVersion) + "/files.txt"));
//    m_manager2->get(request);


//}

//void WelcomeScreen::downloadUpdateFiles(QString filepath)
//{
//    QString wgetpath = "";
//    QStringList parameters;
//    QString currentPath = QFileInfo(QCoreApplication::applicationFilePath()).absolutePath();
//    currentPath = currentPath + "/updates";
//    #if defined(WIN32)
//       wgetpath = "C:/Program Files (x86)/GnuWin32/bin/wget";
//       parameters << "-P" << currentPath;
//       parameters << "-i" << filepath;
//       parameters << "--no-check-certificate";
//    #else
//       wgetpath = "/usr/bin/wget";
//       parameters << "-P" << currentPath;
//       parameters << "-i" << filepath;
//       parameters << "--no-check-certificate";
//    #endif

//    int exitCode = QProcess::execute(wgetpath, parameters);
//    if (exitCode==0) {
//        qDebug() << "HASCH";
//    } else {
//        qDebug() << "MASCH!";
//    }
//}

void WelcomeScreen::readPendingDatagrams()
{
    qDebug() << "WHOOP!";
    while (m_udpServer->hasPendingDatagrams()) {
        QByteArray buffer;
        buffer.resize(m_udpServer->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;
        m_udpServer->readDatagram(buffer.data(), buffer.size(),&sender, &senderPort);
        qDebug() << QString(buffer.data());
    }
    foreach(QWidget *w, qApp->topLevelWidgets()) {
        if (QString(w->metaObject()->className()) != "WelcomeScreen") w->close();
    }
    this->show();
    this->setWindowState(Qt::WindowMinimized);
    this->setWindowState(Qt::WindowActive);
    QString cmd = "killall xvkbd";
    QProcess::startDetached(cmd);

}
