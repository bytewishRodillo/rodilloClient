#include "networkconnectiondialogwifi1.h"
#include "ui_networkconnectiondialogwifi1.h"
#include <QStyle>
#include <QDesktopWidget>
#include <QStringListModel>
#include <QListView>
#include <QStringList>
#include <QProcess>
#include <QMessageBox>
#include <QTimer>
#include "networkconnectiondialogip.h"

NetworkConnectionDialogWifi1::NetworkConnectionDialogWifi1(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NetworkConnectionDialogWifi1)
{
    ui->setupUi(this);
    m_parentWindow = parent;

    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->setGeometry(
        QStyle::alignedRect(
            Qt::RightToLeft,
            Qt::AlignTop,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );


    connect(ui->btnBack, SIGNAL(clicked(bool)), this, SLOT(openParentWindow()));
    m_networkconfig = new BWNetworkConfiguration();

    // Sprachen
    if(UI_LANGUAGE == "deu") {
        ui->label->setText("Verfügbare Netzwerke");
        ui->btnBack->setText("Zurück");
        ui->btnUpdate->setText("Aktualisieren");
        ui->btnConnect->setText("Verbinden");
        ui->txtPassword->setPlaceholderText("Passwort");
    }

    updateWifis();
    QString cmd = "/usr/bin/xvkbd -geometry +1+265";
    QProcess::startDetached(cmd);

}

NetworkConnectionDialogWifi1::~NetworkConnectionDialogWifi1()
{
    delete ui;
}

void NetworkConnectionDialogWifi1::openParentWindow()
{
    QString cmd = "killall xvkbd";
    QProcess::startDetached(cmd);
    m_parentWindow->show();
    this->close();
}

void NetworkConnectionDialogWifi1::on_lstWifis_clicked(const QModelIndex &index)
{
    QStringListModel* listModel= qobject_cast<QStringListModel*>(ui->lstWifis->model());
    if(listModel->rowCount() > 0) {
        QString value = listModel->stringList().at(index.row());
        ui->txtSSID->setText(value);
        ui->txtPassword->setFocus();
    }
}

void NetworkConnectionDialogWifi1::updateWifis()
{
    QStringList *newlist = m_networkconfig->scanWirelessNetworks();
    QStringListModel *newmodel = new QStringListModel();
    newmodel->setStringList(*newlist);
    ui->lstWifis->setModel(newmodel);
}

void NetworkConnectionDialogWifi1::on_btnUpdate_clicked()
{
    updateWifis();
}

void NetworkConnectionDialogWifi1::on_btnConnect_clicked()
{
//    QString *ssid = new QString(ui->txtSSID->text());
//    QString *pass = new QString(ui->txtPassword->text());
//    m_networkconfig->connectToWifi(ssid, pass, true);
    checkWifiConnection();
}

void NetworkConnectionDialogWifi1::checkWifiConnection()
{
    NetworkConnectionDialogIP *ipscreen = new NetworkConnectionDialogIP(this, "wlan0", ui->txtSSID->text(), ui->txtPassword->text());
    ipscreen->show();
    this->hide();
}
