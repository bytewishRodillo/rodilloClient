#include "systeminfoscreen.h"
#include "ui_systeminfoscreen.h"
#include <QStyle>
#include <QDesktopWidget>
#include <QDebug>
#include <QTimer>
#include <QUrlQuery>
#include <QFileInfo>
#include <QFile>
#include <QDir>
#include <QProcess>
#include <QList>
#include <QNetworkInterface>

SystemInfoScreen::SystemInfoScreen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SystemInfoScreen)
{
    ui->setupUi(this);
    m_parentWindow = parent;

    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
    m_networkconfig = new BWNetworkConfiguration();

    connect(ui->btnBack, SIGNAL(clicked(bool)), this, SLOT(openParentWindow()));

    // Sprachen
    if(UI_LANGUAGE == "deu") {
        ui->btnBack->setText("Zurück");
    }

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(checkNetwork()));
    timer->start(5000);

    myDns = new QDnsLookup(this);
    connect(myDns, SIGNAL(finished()),this, SLOT(handleDnsTest()));

    loadCertificates();

    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

    checkNetwork();
    checkIPAddresses();
}

SystemInfoScreen::~SystemInfoScreen()
{
    delete ui;
}

void SystemInfoScreen::openParentWindow()
{
    m_parentWindow->show();
    this->close();
}

bool SystemInfoScreen::checkPing(QString pHostname)
{
    QStringList parameters;
    #if defined(WIN32)
       parameters << "-n" << "1";
    #else
       parameters << "-c 1";
    #endif

       parameters << pHostname;

       int exitCode = QProcess::execute("ping", parameters);
       if (exitCode==0) {
           return true;
       } else {
           return false;
       }
}

void SystemInfoScreen::handleDnsTest()
{
    if (myDns->error() != QDnsLookup::NoError) {
        ui->lblDNS->setText("ERR");
        ui->lblDNS->setStyleSheet("QLabel { color: red; }");
    } else {
        ui->lblDNS->setText("OK");
        ui->lblDNS->setStyleSheet("QLabel { color: green; }");
    }
}

void SystemInfoScreen::checkNetwork()
{
    QString conwifi = m_networkconfig->getConnectedWifi();
    ui->lblConnected->setText("WLAN (" + conwifi + ")");
    // PING
    QString tHostname = "8.8.8.8";
    QString tDnsHost = "www.heise.de";

    if(checkPing(tHostname)) {
        ui->lblInternet->setText("OK");
        ui->lblInternet->setStyleSheet("QLabel { color: green; }");
    } else {
        ui->lblInternet->setText("ERR");
        ui->lblInternet->setStyleSheet("QLabel { color: red; }");
    }

    // DNS
    myDns->setType(QDnsLookup::A);
    myDns->setName(tDnsHost);
    myDns->lookup();

    // BACKEND
    QNetworkRequest request;
    request.setSslConfiguration(m_sslConfiguration);
    QString m_version = QString(VERSION_BUILD);
    QString m_serial = "2";

    request.setUrl(QUrl("https://rodillo.bytewish.de/checkCommunication.php?version=" + m_version + "&sn=" + m_serial));
    m_manager->get(request);


}

void SystemInfoScreen::replyFinished(QNetworkReply *reply)
{
    if(reply->error())
        {
            qDebug() << "ERROR!";
            qDebug() << reply->errorString();

            ui->lblBytewish->setText("ERR: " + reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString());
            ui->lblBytewish->setStyleSheet("QLabel { color: red; }");
            //QTimer::singleShot(m_loginTryInterval * 1000, this, SLOT(doLogin()));
        }
        else
        {
            //qDebug() << reply->header(QNetworkRequest::ContentTypeHeader).toString();
            //qDebug() << reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();
            //qDebug() << reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
            //qDebug() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
            //qDebug() << reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    //        qDebug() << "HAHN";
    //        qDebug() << reply->readAll();
    //        qDebug() << "HUHN";

            // Command request was successful
            // Check if there is anything to process in the reply
            QString replysource = reply->readAll();
            if(replysource.contains("<OK>")) {
                ui->lblBytewish->setText("OK");
                ui->lblBytewish->setStyleSheet("QLabel { color: green; }");
            } else {
                ui->lblBytewish->setText(replysource);
                ui->lblBytewish->setStyleSheet("QLabel { color: red; }");
            }

        }

        reply->deleteLater();
}

void SystemInfoScreen::loadCertificates()
{
        QString certlocation = "";
        #if defined(WIN32)
           certlocation = "C:\\bytewish\\";
        #else
           certlocation = "/data/rodillo/";
        #endif

        // DEVICE CERT
        QFile certFile1(certlocation + "client.pem");
        certFile1.open(QFile::ReadOnly);
        m_deviceCertificate = new QSslCertificate(&certFile1);
        //qWarning() << m_deviceCertificate->isNull();
        certFile1.close();

        // DEVICE KEY
        QFile keyFile(certlocation + "client.key");
        keyFile.open(QFile::ReadOnly);
        m_deviceKey = new QSslKey(&keyFile, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
        //qWarning() << m_deviceKey->isNull();
        keyFile.close();

        // SIGNING CA CERT
        QFile certFile(certlocation + "sign_ca.pem");
        certFile.open(QFile::ReadOnly);
        m_signingCACertificate = new QSslCertificate(&certFile);
        //qWarning() << m_signingCACertificate->isNull();
        certFile.close();

        // REMOTE CA CERT
        QFile certFile2(certlocation + "remote_ca.pem");
        certFile2.open(QFile::ReadOnly);
        m_remoteCACertificate = new QSslCertificate(&certFile2);
        //qWarning() << m_remoteCACertificate->isNull();
        certFile2.close();

        // REMOTE INTERMEDIATE CA CERT
        QFile certFile3(certlocation + "remote_intermediate_ca.pem");
        certFile3.open(QFile::ReadOnly);
        m_remoteIntermediateCACertificate = new QSslCertificate(&certFile3);
        //qWarning() << m_remoteIntermediateCACertificate->isNull();
        certFile3.close();

        // Build SSL Configuration
        QSslConfiguration configuration;

        configuration.setLocalCertificate(*m_deviceCertificate);
        configuration.setPrivateKey(*m_deviceKey);

        QList<QSslCertificate> certs;
        certs.append(*m_signingCACertificate);
        certs.append(*m_remoteCACertificate);
        certs.append(*m_remoteIntermediateCACertificate);
        configuration.setCaCertificates(certs);

        m_sslConfiguration = configuration;
}

void SystemInfoScreen::checkIPAddresses()
{
    QList<QNetworkInterface> allInterfaces = QNetworkInterface::allInterfaces();
    QNetworkInterface eth;

    foreach(eth, allInterfaces) {
        QNetworkInterface::InterfaceFlags flags = eth.flags();
        if( (bool)(flags & QNetworkInterface::IsRunning) && !(bool)(flags & QNetworkInterface::IsLoopBack)) {
            QList<QNetworkAddressEntry> allEntries = eth.addressEntries();
            QNetworkAddressEntry entry;
            foreach (entry, allEntries) {
                if(entry.ip().protocol() == QAbstractSocket::IPv4Protocol && (entry.ip().toString().indexOf("169.", 0) < 0)) {
                    //qDebug() << entry.ip().toString() << "/" << entry.netmask().toString();
                    ui->lblIP->setText(entry.ip().toString());
                    ui->lblNetmask->setText(entry.netmask().toString());
                }
            }
        }
    }
}
