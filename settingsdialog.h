#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QtNetwork/QDnsLookup>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QUdpSocket>
#include <QSsl>
#include <QSslKey>
#include <QNetworkReply>
#include "bwglobals.h"
#include "bwnetworkconfiguration.h"
#include <QFile>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0, QWidget *p_welcomescreen = 0);
    ~SettingsDialog();
public slots:
    void openWelcomeWindow();

private slots:


    void on_btnConfigNetwork_clicked();
    void on_btnViewSettings_clicked();
    void on_btnFactory_clicked();
    void loadCertificates();
    void replyFinished(QNetworkReply *reply);
    void removePicturesFromShow();
    void resetToFactory(bool removePhotosFromCloud = false);

private:

    Ui::SettingsDialog *ui;
    QWidget *m_welcomescreen;
    BWNetworkConfiguration *m_networkconfig;

    QNetworkAccessManager *m_manager;
    QSslConfiguration m_sslConfiguration;
    QSslCertificate *m_deviceCertificate;
    QSslKey *m_deviceKey;
    QSslCertificate *m_remoteCACertificate;             // CA-Certificate of the connected URL (the server certificate ca for https://services.bytewish.de)
    QSslCertificate *m_remoteIntermediateCACertificate; // Intermediate CA certificate (for example the startssl intermediate certificate)
    QSslCertificate *m_signingCACertificate;            // Public CA cert of the bytewish device ca
};

#endif // SETTINGSDIALOG_H
