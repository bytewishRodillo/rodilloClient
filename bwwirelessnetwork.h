#ifndef BWWIRELESSNETWORK_H
#define BWWIRELESSNETWORK_H

#include <QObject>

class BWWirelessNetwork : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString ssid READ ssid WRITE setSsid)
    Q_PROPERTY(QString password READ password WRITE setPassword)
    Q_PROPERTY(QString encryption READ encryption WRITE setEncryption)
    Q_PROPERTY(int priority READ priority WRITE setPriority)

public:
    explicit BWWirelessNetwork(QObject *parent = nullptr);
    explicit BWWirelessNetwork(QString ssid, QString password, QString encryption="WPA-PSK", int priority=-1, QObject *parent = nullptr);

    QString ssid() const
    {   return m_ssid;    }

    QString password() const
    {   return m_password;    }

    QString encryption() const
    {   return m_encryption;    }

    int priority() const
    {   return m_priority;    }

    void setSsid(QString ssid)
    {
        m_ssid = ssid;
    }

    void setPassword(QString password)
    {
        m_password = password;
    }

    void setEncryption(QString encryption)
    {
        m_encryption = encryption;
    }

    void setPriority(int priority)
    {
        m_priority = priority;
    }

private:
    QString m_ssid, m_password, m_encryption;
    int m_priority;

signals:

public slots:
};

#endif // BWWIRELESSNETWORK_H
