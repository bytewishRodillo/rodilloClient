#ifndef WELCOMESCREEN_H
#define WELCOMESCREEN_H


#include <QMainWindow>
#include "bwglobals.h"
#include "shutdowndialog.h"
#include "settingsdialog.h"
#include "qrcodedialog.h"
#include "slideshowsettingsdialog.h"
#include <QtNetwork/QDnsLookup>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QUdpSocket>
#include <QSsl>
#include <QSslKey>
#include <QNetworkReply>

namespace Ui {
class WelcomeScreen;
}

class WelcomeScreen : public QMainWindow
{
    Q_OBJECT
public slots:
    void openShutdownWindow();
    void openSettingsWindow();
    void openSlideshowSettingsWindow();
    void openQrCodeWindow();

//    void replyFinished2 (QNetworkReply *reply);
//    void updateClient();
//    void downloadAndProcessUpdate(int pVersion);
//    void downloadUpdateFiles(QString filepath);
    void readPendingDatagrams();

public:
    explicit WelcomeScreen(QWidget *parent = 0);
    ~WelcomeScreen();

private slots:
    void on_btnCloseMenu_clicked();

private:
    Ui::WelcomeScreen *ui;
    ShutdownDialog *myShutdownDialog;
    SettingsDialog *mySettingsDialog;
    QrCodeDialog *myQrCodeDialog;
    SlideshowSettingsDialog *mySlideshowSettingsDialog;


//    QNetworkAccessManager *m_manager2;
    QUdpSocket *m_udpServer;
//    void loadCertificates();
//    void readVersion();
//    bool lastUrlWasFilelist;
//    QSslConfiguration m_sslConfiguration;
//    QSslCertificate *m_deviceCertificate;
//    QSslKey *m_deviceKey;
//    QSslCertificate *m_remoteCACertificate;             // CA-Certificate of the connected URL (the server certificate ca for https://services.bytewish.de)
//    QSslCertificate *m_remoteIntermediateCACertificate; // Intermediate CA certificate (for example the startssl intermediate certificate)
//    QSslCertificate *m_signingCACertificate;            // Public CA cert of the bytewish device ca

};

#endif // WELCOMESCREEN_H
