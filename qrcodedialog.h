#ifndef QRCODEDIALOG_H
#define QRCODEDIALOG_H

#include <QDialog>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QSsl>
#include <QSslKey>
#include "bwglobals.h"

namespace Ui {
class QrCodeDialog;
}

class QrCodeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QrCodeDialog(QWidget *parent = 0, QWidget *p_welcomescreen = 0);
    ~QrCodeDialog();

public slots:
    void openWelcomeWindow();
    void downloadFinished(QNetworkReply *reply);

private:
    Ui::QrCodeDialog *ui;
    QWidget *m_welcomescreen;
    void loadCertificates();
    QSslConfiguration m_sslConfiguration;
    QSslCertificate *m_deviceCertificate;
    QSslKey *m_deviceKey;
    QSslCertificate *m_remoteCACertificate;             // CA-Certificate of the connected URL (the server certificate ca for https://services.bytewish.de)
    QSslCertificate *m_remoteIntermediateCACertificate; // Intermediate CA certificate (for example the startssl intermediate certificate)
    QSslCertificate *m_signingCACertificate;            // Public CA cert of the bytewish device ca
};

#endif // QRCODEDIALOG_H
