#include "qrcodedialog.h"
#include "ui_qrcodedialog.h"
#include <QStyle>
#include <QDesktopWidget>
#include <QDebug>
#include <QFile>

QrCodeDialog::QrCodeDialog(QWidget *parent, QWidget *p_welcomescreen) :
    QDialog(parent),
    ui(new Ui::QrCodeDialog)
{

    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->m_welcomescreen = p_welcomescreen;
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );

    connect(ui->btnBack, SIGNAL(clicked(bool)), this, SLOT(openWelcomeWindow()));
    loadCertificates();

    //Sprachen
    if(UI_LANGUAGE == "deu") {
        ui->lblQrCode->setText("                                     Bitte warten...");
        ui->btnBack->setText("Zurück");
    }


    const QUrl url = QUrl("http://rodillo.bytewish.de/get_qrcode.php");
    QNetworkRequest request;
    request.setSslConfiguration(m_sslConfiguration);
    request.setUrl(url);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    QNetworkAccessManager *nam = new QNetworkAccessManager(this);
    connect(nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(downloadFinished(QNetworkReply*)));

    nam->get(request);
}

QrCodeDialog::~QrCodeDialog()
{
    delete ui;
}

void QrCodeDialog::openWelcomeWindow()
{
    m_welcomescreen->show();
    this->close();
}

void QrCodeDialog::downloadFinished(QNetworkReply *reply)
{
    if(reply->error())
        {
            qDebug() << "ERROR!";
            qDebug() << reply->errorString();

            //QTimer::singleShot(m_loginTryInterval * 1000, this, SLOT(doLogin()));
        }
        else
        {
            //qDebug() << reply->header(QNetworkRequest::ContentTypeHeader).toString();
            //qDebug() << reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();
            //qDebug() << reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
            qDebug() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
            qDebug() << reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    //        qDebug() << "HAHN";
    //        qDebug() << reply->readAll();
    //        qDebug() << "HUHN";
    }

    QPixmap pm;

    pm.loadFromData(reply->readAll());
    ui->lblQrCode->setPixmap(pm);
}

void QrCodeDialog::loadCertificates()
{
        QString certlocation = "";
        #if defined(WIN32)
           certlocation = "C:\\bytewish\\";
        #else
           certlocation = "/data/rodillo/";
        #endif

        // DEVICE CERT
        QFile certFile1(certlocation + "client.pem");
        certFile1.open(QFile::ReadOnly);
        m_deviceCertificate = new QSslCertificate(&certFile1);
        //qWarning() << m_deviceCertificate->isNull();
        certFile1.close();

        // DEVICE KEY
        QFile keyFile(certlocation + "client.key");
        keyFile.open(QFile::ReadOnly);
        m_deviceKey = new QSslKey(&keyFile, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
        //qWarning() << m_deviceKey->isNull();
        keyFile.close();

        // SIGNING CA CERT
        QFile certFile(certlocation + "sign_ca.pem");
        certFile.open(QFile::ReadOnly);
        m_signingCACertificate = new QSslCertificate(&certFile);
        //qWarning() << m_signingCACertificate->isNull();
        certFile.close();

        // REMOTE CA CERT
        QFile certFile2(certlocation + "remote_ca.pem");
        certFile2.open(QFile::ReadOnly);
        m_remoteCACertificate = new QSslCertificate(&certFile2);
        //qWarning() << m_remoteCACertificate->isNull();
        certFile2.close();

        // REMOTE INTERMEDIATE CA CERT
        QFile certFile3(certlocation + "remote_intermediate_ca.pem");
        certFile3.open(QFile::ReadOnly);
        m_remoteIntermediateCACertificate = new QSslCertificate(&certFile3);
        //qWarning() << m_remoteIntermediateCACertificate->isNull();
        certFile3.close();

        // Build SSL Configuration
        QSslConfiguration configuration;

        configuration.setLocalCertificate(*m_deviceCertificate);
        configuration.setPrivateKey(*m_deviceKey);

        QList<QSslCertificate> certs;
        certs.append(*m_signingCACertificate);
        certs.append(*m_remoteCACertificate);
        certs.append(*m_remoteIntermediateCACertificate);
        configuration.setCaCertificates(certs);

        m_sslConfiguration = configuration;
}
