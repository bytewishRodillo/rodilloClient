#ifndef SHUTDOWNDIALOG_H
#define SHUTDOWNDIALOG_H

#include <QDialog>
#include "bwglobals.h"


namespace Ui {
class ShutdownDialog;
}

class ShutdownDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ShutdownDialog(QWidget *parent = 0, QWidget *p_welcomescreen = 0);
    ~ShutdownDialog();

public slots:
    void openWelcomeWindow();
    void shutdownDevice();
    void restartDevice();

private:
    Ui::ShutdownDialog *ui;
    QWidget *m_welcomescreen;
};

#endif // SHUTDOWNDIALOG_H
