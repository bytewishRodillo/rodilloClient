#ifndef BWNETWORKCONFIGURATION_H
#define BWNETWORKCONFIGURATION_H

#include <QObject>
#include "bwwirelessnetwork.h"
#include <QList>
#include <QStringList>

class BWNetworkConfiguration : public QObject
{
    Q_OBJECT
public:
    explicit BWNetworkConfiguration(QObject *parent = nullptr);
    bool connectToWifi(QString *ssid, QString *password, bool overWriteAllExisting = false);
    bool removeWifi(QString *ssid);
    bool disableWifi();
    QList<BWWirelessNetwork *> *getAllWirelessNetworks();
    bool writeConfigToFile(QList<BWWirelessNetwork *> *listOfWirelessNetworks);
    QStringList *scanWirelessNetworks();
    bool reconfigureWifi();
    bool restartNetworking();

    bool setStaticIp(QString p_interface, QString p_ip, QString p_gateway, QString p_subnetmask, QString p_dns1 = "9.9.9.9", QString p_dns2 = "8.8.8.8");
    bool removeStaticIps();

    QString getConnectedWifi();

    unsigned short toCidr(char* ipAddress);

private:
    QString m_wpa_file;
    QString m_dhcpc_file;
    QString m_dhcpc_destfile;


signals:

public slots:
};

#endif // BWNETWORKCONFIGURATION_H
