#-------------------------------------------------
#
# Project created by QtCreator 2018-03-12T14:35:17
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = rodilloClient
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    slideshowsettingsdialog.cpp \
        welcomescreen.cpp \
    shutdowndialog.cpp \
    settingsdialog.cpp \
    bwglobals.cpp \
    qrcodedialog.cpp \
    bwnetworkconfiguration.cpp \
    bwwirelessnetwork.cpp \
    networkconnectiondialogmain.cpp \
    networkconnectiondialogwifi1.cpp \
    systeminfoscreen.cpp \
    networkconnectiondialogip.cpp

HEADERS += \
    slideshowsettingsdialog.h \
        welcomescreen.h \
    shutdowndialog.h \
    settingsdialog.h \
    bwglobals.h \
    qrcodedialog.h \
    bwnetworkconfiguration.h \
    bwwirelessnetwork.h \
    networkconnectiondialogmain.h \
    networkconnectiondialogwifi1.h \
    systeminfoscreen.h \
    networkconnectiondialogip.h

FORMS += \
    slideshowsettingsdialog.ui \
        welcomescreen.ui \
    shutdowndialog.ui \
    settingsdialog.ui \
    qrcodedialog.ui \
    networkconnectiondialogmain.ui \
    networkconnectiondialogwifi1.ui \
    systeminfoscreen.ui \
    networkconnectiondialogip.ui

DISTFILES += \
    start_rodillo.py
