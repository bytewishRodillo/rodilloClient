#include "shutdowndialog.h"
#include "ui_shutdowndialog.h"
#include <QStyle>
#include <QDesktopWidget>
#include <QDebug>
#include <QSysInfo>
#include <QProcess>

ShutdownDialog::ShutdownDialog(QWidget *parent, QWidget *p_welcomescreen) :
    QDialog(parent),
    ui(new Ui::ShutdownDialog)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->m_welcomescreen = p_welcomescreen;
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );

    connect(ui->btnCancel, SIGNAL(clicked(bool)), this, SLOT(openWelcomeWindow()));
    connect(ui->btnRestart, SIGNAL(clicked(bool)), this, SLOT(restartDevice()));
    connect(ui->btnShutdown, SIGNAL(clicked(bool)), this, SLOT(shutdownDevice()));

    // Sprachen
    if(UI_LANGUAGE == "deu") {
        ui->lbl1->setText("Was willst du tun?");
        ui->btnCancel->setText("Abbrechen");
        ui->btnRestart->setText("Rahmen neu starten");
        ui->btnShutdown->setText("Gerät herunterfahren");
    }
}

ShutdownDialog::~ShutdownDialog()
{
    delete ui;
}

void ShutdownDialog::openWelcomeWindow()
{
    m_welcomescreen->show();
    this->close();
}

void ShutdownDialog::shutdownDevice()
{
    QString cmd = "";
    if(QString::compare(QSysInfo::productType(), "windows", Qt::CaseInsensitive) == 0) {
        // WINDOWS
        cmd = "shutdown -s -f -t 0";
    } else {
        // OTHER
        cmd = "sudo shutdown -h now";
    }
    QProcess::startDetached(cmd);
}

void ShutdownDialog::restartDevice()
{
    QString cmd = "";
    if(QString::compare(QSysInfo::productType(), "windows", Qt::CaseInsensitive) == 0) {
        // WINDOWS
        cmd = "shutdown -r -f -t 0";
    } else {
        // OTHER
        cmd = "sudo shutdown -r now";
    }
    QProcess::startDetached(cmd);
}
