#ifndef SYSTEMINFOSCREEN_H
#define SYSTEMINFOSCREEN_H

#include <QDialog>
#include <QtNetwork/QDnsLookup>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QUdpSocket>
#include <QSsl>
#include <QSslKey>
#include <QNetworkReply>
#include "bwglobals.h"
#include "bwnetworkconfiguration.h"


namespace Ui {
class SystemInfoScreen;
}

class SystemInfoScreen : public QDialog
{
    Q_OBJECT

public:
    explicit SystemInfoScreen(QWidget *parent = 0);
    ~SystemInfoScreen();

public slots:
    void openParentWindow();
    void checkNetwork();
    void handleDnsTest();
    void replyFinished (QNetworkReply *reply);
    void checkIPAddresses();

private:
    Ui::SystemInfoScreen *ui;
    QWidget *m_parentWindow;

    bool checkPing(QString pHostname);
    QDnsLookup *myDns;
    QNetworkAccessManager *m_manager;
    void loadCertificates();
    QSslConfiguration m_sslConfiguration;
    QSslCertificate *m_deviceCertificate;
    QSslKey *m_deviceKey;
    QSslCertificate *m_remoteCACertificate;             // CA-Certificate of the connected URL (the server certificate ca for https://services.bytewish.de)
    QSslCertificate *m_remoteIntermediateCACertificate; // Intermediate CA certificate (for example the startssl intermediate certificate)
    QSslCertificate *m_signingCACertificate;            // Public CA cert of the bytewish device ca

    BWNetworkConfiguration *m_networkconfig;
};

#endif // SYSTEMINFOSCREEN_H
