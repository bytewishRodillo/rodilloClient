#include "networkconnectiondialogmain.h"
#include "ui_networkconnectiondialogmain.h"
#include <QStyle>
#include <QDesktopWidget>
#include "networkconnectiondialogip.h"
#include "networkconnectiondialogwifi1.h"


NetworkConnectionDialogMain::NetworkConnectionDialogMain(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NetworkConnectionDialogMain)
{
    ui->setupUi(this);
    m_parentWindow = parent;

    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );

    connect(ui->btnBack, SIGNAL(clicked(bool)), this, SLOT(openParentWindow()));

    // Sprachen
    if(UI_LANGUAGE == "deu") {
        ui->btnBack->setText("Zurück");
        ui->btnConnectLAN->setText("Verbindung über Netzwerkkabel");
        ui->btnConnectWifi->setText("Verbindung über WLAN (drahtlos)");
        ui->btnConnectMobile->setText("Verbindung über UMTS-Stick (nur möglich wenn gesteckt)");
    }
}

NetworkConnectionDialogMain::~NetworkConnectionDialogMain()
{
    delete ui;
}

void NetworkConnectionDialogMain::openParentWindow()
{
    m_parentWindow->show();
    this->close();
}

void NetworkConnectionDialogMain::on_btnConnectWifi_clicked()
{
    NetworkConnectionDialogWifi1 *wifiwindow = new NetworkConnectionDialogWifi1(this);
    wifiwindow->show();
    this->hide();
}

void NetworkConnectionDialogMain::on_btnConnectLAN_clicked()
{
    NetworkConnectionDialogIP *ipdialog = new NetworkConnectionDialogIP(this, "eth0");
    ipdialog->show();
    this->hide();
}
