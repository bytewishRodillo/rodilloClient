#ifndef NETWORKCONNECTIONDIALOGWIFI1_H
#define NETWORKCONNECTIONDIALOGWIFI1_H

#include <QDialog>
#include "bwnetworkconfiguration.h"
#include "bwglobals.h"

namespace Ui {
class NetworkConnectionDialogWifi1;
}

class NetworkConnectionDialogWifi1 : public QDialog
{
    Q_OBJECT

public:
    explicit NetworkConnectionDialogWifi1(QWidget *parent = 0);
    ~NetworkConnectionDialogWifi1();

public slots:
    void openParentWindow();
    void updateWifis();

private slots:
    void on_lstWifis_clicked(const QModelIndex &index);

    void on_btnUpdate_clicked();
    void on_btnConnect_clicked();
    void checkWifiConnection();

private:
    Ui::NetworkConnectionDialogWifi1 *ui;
    QWidget *m_parentWindow;
    BWNetworkConfiguration *m_networkconfig;
};

#endif // NETWORKCONNECTIONDIALOGWIFI1_H
