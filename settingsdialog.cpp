#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QStyle>
#include <QDesktopWidget>
#include <QDebug>
#include <QMessageBox>
#include <QProcess>

#include "networkconnectiondialogmain.h"
#include "systeminfoscreen.h"

SettingsDialog::SettingsDialog(QWidget *parent, QWidget *p_welcomescreen) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    m_welcomescreen = p_welcomescreen;
    this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );

    connect(ui->btnBack, SIGNAL(clicked(bool)), this, SLOT(openWelcomeWindow()));

    // Sprachen
    if(UI_LANGUAGE == "deu") {
        ui->btnBack->setText("Zurück");
        ui->btnFactory->setText("Auf Werkseinstellungen zurücksetzen");
        ui->btnViewSettings->setText("Systeminformationen anzeigen");
        ui->btnConfigNetwork->setText("Netzwerkverbindung konfigurieren");
    }

    m_networkconfig = new BWNetworkConfiguration();

    loadCertificates();

    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

//    QList <BWWirelessNetwork*> *templist = m_networkconfig->getAllWirelessNetworks();
//    qDebug() << QString::number(templist->count());
//    BWWirelessNetwork *fisch = templist->at(0);
//    qDebug() << fisch->ssid();
//    qDebug() << fisch->encryption();

    //QString tssid = "Brot";
    //QString tpass = "lecker400";
    //m_networkconfig->connectToWifi(&tssid, &tpass);

    //m_networkconfig->removeWifi(&tssid);

}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::openWelcomeWindow()
{
    m_welcomescreen->show();
    this->close();
}

void SettingsDialog::on_btnConfigNetwork_clicked()
{
    NetworkConnectionDialogMain *networkDialog = new NetworkConnectionDialogMain(this);
    networkDialog->show();
    this->hide();
}

void SettingsDialog::on_btnViewSettings_clicked()
{
    SystemInfoScreen *infoScreen = new SystemInfoScreen(this);
    infoScreen->show();
    this->hide();
}

void SettingsDialog::on_btnFactory_clicked()
{
    // Soll wirklich auf Werkseinstellungen zurückgesetzt werden?
    QMessageBox::StandardButton reply;
    QString messageHeader = "";
    QString messageText = "";
    QString messageHeader2 = "";
    QString messageText2 = "";
    if(UI_LANGUAGE == "deu") {
        messageHeader = "Zurücksetzen auf Werkszustand";
        messageText = "Willst du das Gerät wirklich auf Werkseinstellungen zurücksetzen? Alle Fotos und Verbindungseinstellungen werden vom Gerät entfernt.";
        messageHeader2 = "Fotos aus der Cloud entfernen?";
        messageText2 = "Willst du auch alle Fotos vom Server entfernen? Diese Funktion ist z.B. dann nützlich, wenn du den Bilderrahmen verkaufen möchtest, ohne dass jemand deine Fotos bekommt. Achtung: Das klappt nur, wenn aktuell eine Internetverbindung besteht! Wenn du dir nicht sicher bist und unbedingt alle Fotos aus der Cloud entfernen willst, schicke bitte eine Nachricht an support@bytewish.de";
    } else {
        messageHeader = "Factory reset";
        messageText = "Do you really want to reset the device to factory default? All photos and the connection information are removed from the device";
        messageHeader2 = "Remove from cloud";
        messageText2 = "Do you want to remove all photos from the cloud service? This comes in handy if you sell this device. Note: This only works if the internet connection is working. If you want to be 100% sure, send a message to support@bytewish.de";
    }
    reply = QMessageBox::question(this, messageHeader, messageText, QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        reply = QMessageBox::question(this, messageHeader2, messageText2, QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            this->resetToFactory(true);
        } else {
            this->resetToFactory(false);
        }
    } else {
        qDebug() << "Yes was *not* clicked";
    }

}

void SettingsDialog::loadCertificates()
{
        QString certlocation = "";
        #if defined(WIN32)
           certlocation = "C:\\bytewish\\";
        #else
           certlocation = "/data/rodillo/";
        #endif

        // DEVICE CERT
        QFile certFile1(certlocation + "client.pem");
        certFile1.open(QFile::ReadOnly);
        m_deviceCertificate = new QSslCertificate(&certFile1);
        //qWarning() << m_deviceCertificate->isNull();
        certFile1.close();

        // DEVICE KEY
        QFile keyFile(certlocation + "client.key");
        keyFile.open(QFile::ReadOnly);
        m_deviceKey = new QSslKey(&keyFile, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
        //qWarning() << m_deviceKey->isNull();
        keyFile.close();

        // SIGNING CA CERT
        QFile certFile(certlocation + "sign_ca.pem");
        certFile.open(QFile::ReadOnly);
        m_signingCACertificate = new QSslCertificate(&certFile);
        //qWarning() << m_signingCACertificate->isNull();
        certFile.close();

        // REMOTE CA CERT
        QFile certFile2(certlocation + "remote_ca.pem");
        certFile2.open(QFile::ReadOnly);
        m_remoteCACertificate = new QSslCertificate(&certFile2);
        //qWarning() << m_remoteCACertificate->isNull();
        certFile2.close();

        // REMOTE INTERMEDIATE CA CERT
        QFile certFile3(certlocation + "remote_intermediate_ca.pem");
        certFile3.open(QFile::ReadOnly);
        m_remoteIntermediateCACertificate = new QSslCertificate(&certFile3);
        //qWarning() << m_remoteIntermediateCACertificate->isNull();
        certFile3.close();

        // Build SSL Configuration
        QSslConfiguration configuration;

        configuration.setLocalCertificate(*m_deviceCertificate);
        configuration.setPrivateKey(*m_deviceKey);

        QList<QSslCertificate> certs;
        certs.append(*m_signingCACertificate);
        certs.append(*m_remoteCACertificate);
        certs.append(*m_remoteIntermediateCACertificate);
        configuration.setCaCertificates(certs);

        m_sslConfiguration = configuration;
}

void SettingsDialog::replyFinished(QNetworkReply *reply)
{
    if(reply->error())
        {
            qDebug() << "ERROR!";
            qDebug() << reply->errorString();

            //QTimer::singleShot(m_loginTryInterval * 1000, this, SLOT(doLogin()));
        }
        else
        {
            //qDebug() << reply->header(QNetworkRequest::ContentTypeHeader).toString();
            //qDebug() << reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();
            //qDebug() << reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
            //qDebug() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
            //qDebug() << reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    //        qDebug() << "HAHN";
    //        qDebug() << reply->readAll();
    //        qDebug() << "HUHN";

            // Command request was successful
            // Check if there is anything to process in the reply
            QString replysource = reply->readAll();
            if(replysource.contains("<OK>")) {
                qDebug() << "OK";
            } else {
                qDebug() << "ERROR";
            }

        }

        reply->deleteLater();
}

void SettingsDialog::removePicturesFromShow()
{
    // BACKEND
    QNetworkRequest request;
    request.setSslConfiguration(m_sslConfiguration);

    request.setUrl(QUrl("https://rodillo.bytewish.de/removePicturesFromShow.php"));
    m_manager->get(request);
}

void SettingsDialog::resetToFactory(bool removePhotosFromCloud)
{
    // Remove all photos from cache
    QProcess::startDetached("sudo find /data/rodillo/cache/ ! -name dummy.jpg -type f -exec sudo rm -f {} +");

    // Remove network configuration
    m_networkconfig->disableWifi();
    m_networkconfig->removeStaticIps();
    // Remove photos from cloud (if chosen)
    if (removePhotosFromCloud) {
        this->removePicturesFromShow();
    }

    QMessageBox msgbox;
    if(UI_LANGUAGE == "deu") {
        msgbox.setText("Das Gerät wurde auf Werkseinstellungen zurückgesetzt und startet nun neu.");
    } else {
        msgbox.setText("The device was set to factory defaults. It is rebooting now.");
    }

    msgbox.exec();

    QProcess::startDetached("sudo shutdown -r now");
}
